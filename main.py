
# coding: utf-8

# In[1]:


from sender import sendEmail2
from downloader import Downloader,returnString,getDict,main


# In[2]:


import glob
import os
import pickle
from pandas import DataFrame, ExcelWriter
from numpy import nan
import datetime


# In[36]:


if __name__ == "__main__":
	try:
		folder = ''
		#Error inducido abajo
##		bbb
		print('Descargando nuevos correos...')
		Downloader('today',folder)
		files = glob.glob('*.eml')
		print('Procesando archivos ".eml"...')
		entrada = main(files)
		print('Creando Dataframe...')
		dfTest = DataFrame.from_dict(entrada)
		print('Procesando Dataframe...')
		a = dfTest.replace('',nan).dropna(subset=['Numero de Serie'])
		a = a.drop_duplicates(subset=['Fecha','Toner'])
		a = a.set_index(['Cliente','Ubicacion','Modelo Impresora',
                         'Numero de Serie','Fecha','Toner','Percentil',
                         'Contador']).sort_index()
		a = a.replace(nan,'')
		print('Grabando pickle...')
		pickle.dump( a, open( "Alertas.pickle", "wb" ) )

		files = glob.glob(folder+'*.eml')
		print('Creando arcvivo Excel...')
		exc = glob.glob(folder+'Alerta*.xlsx')
		writer = ExcelWriter(folder+'Alerta{}.xlsx'.format(len(exc)))
		a.to_excel(writer,'Sheet1')
		writer.save()
		
		print('limpiando carpeta...')
		for f in files:
			os.remove(f)
		
		default_email = 'alan.reyna@microplus.com.mx'
		print(f'enviando a <{default_email}>')
		emails = [default_email]
		print('Cargando pickle...')
		a = pickle.load(open("Alertas.pickle",'rb'))
		a = a.to_html()
		
		for i in emails:
			sendEmail2('alertas',i,a)
		print('Operación Exitosa!')
	except Exception as e:
		print()
		f = open('log.txt','a+')
		f.write('Error ({}): {}\n'.format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),e))
		f.close()
		a = input('Error! Checar log para más detalles (pica Enter para seguir)')

