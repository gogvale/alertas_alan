
# coding: utf-8

# # EmailWriter
#     Escribe correos a partir de la información recibida por el
#     alertaimpresiones
#     SI posible, hacerlo con boton de aprobacion para seguir con el codigo
#     a soporte tecnico

# In[1]:


# # Mandar correo 

# In[2]:


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from configparser import ConfigParser
       
        
def sendEmail2(template,to,dfhtml):
    config = ConfigParser()
    config.read('Email.ini')

    me = config.get('Outlook_SMTP', 'User')
    pw = config.get('Outlook_SMTP', 'Pw')
    server = '{}:{}'.format(config.get('Outlook_SMTP', 'Host'),config.get('Outlook_SMTP', 'Puerto'))
    
    if template == 'null':
        subject = 'Mensajes Vacios'

        text = """
        Buenos días, ese es un mensaje automático, favor de no contestar.
        
        Se recibieron correos sin encabezados, aquí el contenido que se pudo recuperar:


        {table}

        Saludos,

        Microplus_BOT™"""

        html = """
        <html><body><p>Buenos días, ese es un mensaje automático, favor de no contestar.</p>
        <p>Se recibieron correos sin encabezados, aquí el contenido que se pudo recuperar:</p>
        <br>
        {table}
        <p>Saludos,</p>
        <p>Microplus_BOT™</p>
        </body></html>
        """

    elif template == 'exportados':
        
        subject = 'Resumen HP Web Jetadmin Exported Device List'

        text = """
        Buenos días, ese es un mensaje automático, favor de no contestar.
        
        Se recibieron correos del tipo "Exported Device List", aquí el resumen de la última hora:


        {table}

        Saludos,

        Microplus_BOT™"""

        html = """
        <html><body><p>Buenos días, ese es un mensaje automático, favor de no contestar.</p>
        <p>Se recibieron correos del tipo "Exported Device List", aquí el resumen de la última hora:</p>
        <br>
        {table}
        <p>Saludos,</p>
        <p>Microplus_BOT™</p>
        </body></html>
        """

    elif template == 'reportes':
        
        subject = 'Resumen HP Web Jetadmin Report'

        text = """
        Buenos días, ese es un mensaje automático, favor de no contestar.
        
        Se recibieron correos de Reporte de Utilización de Servicios, aquí el resumen de la última hora:


        {table}

        Saludos,

        Microplus_BOT™"""

        html = """
        <html><body><p>Buenos días, ese es un mensaje automático, favor de no contestar.</p>
        <p>Se recibieron correos de Reporte de Utilización de servicios, aquí el resumen de la última hora:</p>
        <br>
        {table}
        <p>Saludos,</p>
        <p>Microplus_BOT™</p>
        </body></html>
        """

    elif template == 'alertas':
        subject = 'Resumen HP Device Alert'

        text = """
        Buenos días, ese es un mensaje automático, favor de no contestar.
        
        Se recibieron correos de Alertas de Impresión, aquí el resumen de la última hora:
        


        {table}

        Saludos,

        Microplus_BOT™"""

        html = """
        <html><body><p>Buenos días, ese es un mensaje automático, favor de no contestar.</p>
        <p>Se recibieron correos de Reporte de Utilización de servicios, aquí el resumen de la última hora:</p>
        <br>
        {table}
        <p>Saludos,</p>
        <p>Microplus_BOT™</p>
        </body></html>
        """




    text = text.format(table=dfhtml)
    html = html.format(table=dfhtml)

    message = MIMEMultipart(
        "alternative", None, [MIMEText(text), MIMEText(html,'html')])

    message['Subject'] = subject
    message['From'] = me
    message['To'] = to
    server = smtplib.SMTP(server)
    server.ehlo()
    server.starttls()
    server.login(me, pw)
    server.sendmail(me, to, message.as_string())
    server.quit()


# # Main

# In[7]:

#if __name__ == "__main__":
#	default_email = 'alan.reyna@microplus.com.mx'
#	emails = [default_email]
#	a = pickle.load(open("Alertas.pickle",'rb'))
#	a = a.to_html()
#	for i in emails:
#		sendEmail2('alertas',i,a)
#
