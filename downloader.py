
# coding: utf-8

# # Index

# # Imports

# In[1]:


from configparser import ConfigParser
import email
import glob
import imaplib
import os
import re
import time


# # Global Variables

# # Download

# In[3]:


def Downloader(mode='all',folder = '../Emails/'):
    """
    Los modos son:
    'all' - checa todos los correos
    'today' - checa los correos de hoy
    'unread' - checa los no leídos

    Todos buscan en la carpeta de Alertas de Microplus
    """
    opciones = {
        'all':'ALL',
        'today':"(ON {})".format(time.strftime("%d-%b-%Y")),
        'unread':'(UNSEEN)'
    }

    files = glob.glob(folder+'*.eml')
    for f in files:
        os.remove(f)

    config = ConfigParser()
    config.read('Email.ini')

    mail = imaplib.IMAP4_SSL(config.get('Outlook', 'Host'))
    # imaplib module implements connection based on IMAPv4 protocol
    mail.login(config.get('Outlook', 'User'), config.get('Outlook', 'Pw'))
    # >> ('OK', [username at gmail.com Vineet authenticated (Success)'])

    # for i in mail.list()[1]:
    #     l = i.decode().split(' "/" ')
    #     print(l[1])

    mail.select('"INBOX/Alertas "')

    result, data = mail.uid('search', None, "{}".format(opciones[mode]))

    raw_email = list()
    # search and return uids instead
    # i = len(data[0].split()) # data[0] is a space separate string
    for i in data[0].split():
        latest_email_uid = i # unique ids wrt label selected
        result, email_data = mail.uid('fetch', latest_email_uid, '(RFC822)')
        # fetch the email body (RFC822) for the given ID
        raw_email.append(email_data[0][1])

    for idx,i in enumerate(raw_email):
        #continue inside the same for loop as above
        raw_email_string = i.decode('utf-8')
        # converts byte literal to string removing b''
        email_message = email.message_from_string(raw_email_string) ## <=================================
        # this will loop through all the available multiparts in mail
        for part in email_message.walk():
            if part.get_content_type() == "text/plain": # ignore attachments/html
                body = part.get_payload(decode=True)
                save_string = str(folder + str(idx) + ".eml")
                # location on disk
                myfile = open(save_string, 'w')
                myfile.write('date->"{}"\n'.format(email_message['date']))
                myfile.write('"{}"\n'.format(email_message['from']))
                myfile.write(body.decode('utf-8'))
                # body is again a byte literal
                myfile.close()
            else:
                continue


# In[40]:


def returnString(regexString,valuesDict,siguiente=False):
    if siguiente:
        tmp = [i for i,j in enumerate(valuesDict) if bool(re.search(regexString,j))]
        if len(tmp) > 0:
            tmp = list(valuesDict)[(tmp[0]+1)]
    else:
        tmp = [i for i in valuesDict if bool(re.search(regexString,i))]
    if len(tmp) > 0:
        tmp = tmp[0]
        
        if '"' in tmp:
            tmp = tmp.split('"')[1]
        else:
            tmp = tmp.split(':')[-1].strip()
        if '@' in tmp:
            tmp = tmp.split('<')[-1].split('>')[0]
            tmp = tmp.split('@')[-1].split('.')[0].title()
        if regexString == 'date->"(.*)"':
            tmp = ':'.join(tmp.split(':')[:-1])
        try:
            tmp = float(tmp)
        except:
            pass
#         print('ok',end='\r')

        return tmp
#     else:
#         print('\n{} not found'.format(regexString))
        return ''


# In[34]:


def getDict(values2,entrada):
    serialNumber = '("\w{10}")'
    count = '(Count|Uso).*"(\d+[.]?\d+)"'
    deviceModel = '("HP.*")'
    toner = '[ \t]+("([A-Z]{1,2}\d[A-Z0-9]{3,}|110V.*)")'
    correo = '([^\s]+[@]\w+\.)'
    date = 'date->"(.*)"'
    perc = 'Threshold.*"(.*)"'
    location = '(Ubicacion del Equipo|System Location|Device Location|Ubicación).*"(.*)"'
    contador = '(Contador Total|Paginas de Uso|Count).*"(.*)"|^(\d+\.\d{2})'



    entrada['Numero de Serie'].append(returnString(serialNumber,values2))
    entrada['Contador'].append(returnString(count,values2))
    entrada['Modelo Impresora'].append(returnString(deviceModel,values2))
    entrada['Toner'].append(returnString(toner,values2))
    entrada['Cliente'].append(returnString(correo,values2))
    entrada['Percentil'].append(returnString(perc,values2))
    entrada['Fecha'].append(returnString(date,values2))
    entrada['Ubicacion'].append(returnString(location,values2))
    a = returnString(contador,values2)
    if type(a) is list:
        a = a[0]
        entrada['Contador'].append(a)
    
#     print(entrada)
    return entrada


# In[6]:


def main(files):
    entrada = {'Numero de Serie': list(),
     'Contador': list(),
     'Modelo Impresora': list(),
     'Toner': list(),
     'Cliente': list(),
     'Percentil': list(),
     'Fecha': list(),
     'Ubicacion': list(),
     'Contador': list()}
    for i in files:

        values2 = [line for line in open(i)]

#         values2 = set()
#         for i in values:
#             try:
#                 values2.add(i[0])
#             except:
#                 pass
        entrada = getDict(values2,entrada)
#         print(entrada)
    return entrada

#if __name__ == "__main__":
#
#	folder = ''
#	Downloader('today',folder)
#	files = glob.glob(folder+'*.eml')
#	entrada = main(files)
#	dfTest = pd.DataFrame.from_dict(entrada)
#
#	a = dfTest.replace('',np.nan).dropna(subset=['Numero de Serie'])
#	a = a.drop_duplicates(subset=['Fecha','Toner'])
#	a = a.set_index(['Cliente','Modelo Impresora','Numero de Serie','Fecha','Toner']).sort_index()
#	a = a.replace(np.nan,'')
#
#	pickle.dump( a, open( "Alertas.pickle", "wb" ) )
#
#	files = glob.glob(folder+'*.eml')
#
#	folder = '..\\..\\'
#	exc = glob.glob(folder+'Alerta*.xlsx')
#	print('{}:{}'.format(len(exc),exc))
#	writer = pd.ExcelWriter(folder+'Alerta{}.xlsx'.format(len(exc)))
#	a.to_excel(writer,'Sheet1')
#	writer.save()
#
#	for f in files:
#		os.remove(f)
#
